#!/usr/bin/python
# encoding: utf-8

import glob
import os
import re

pattern = re.compile('[\s_]+')
def remove_nonalfa(s):
    #return pattern.sub('', s)
    return s.strip()

def port_list():
    pl = glob.glob('/dev/ttyACM*') + glob.glob('/dev/ttyUSB*')
    return pl

def read_line(file_name):
    f = open(file_name)
    return f.readline()

def usb_sysfs_friendly_name(sys_usb_path):
    devnum       = remove_nonalfa(read_line( sys_usb_path + "/devnum"))
    manufacturer = remove_nonalfa(read_line( sys_usb_path + "/manufacturer" ))
    product      = remove_nonalfa(read_line( sys_usb_path + "/product" ))
    serial       = remove_nonalfa(read_line( sys_usb_path + "/serial" ))

    fr_name = "{} {}.".format(manufacturer, product)
    return fr_name

def usb_sysfs_hw_string(sys_usb_path):
    serial_number = remove_nonalfa(read_line( sys_usb_path + "/serial" ))
    if len(serial_number) > 0 :
        serial_number = "SNR={}".format(serial_number)
    vid = remove_nonalfa(read_line( sys_usb_path + "/idVendor" ))
    pid = remove_nonalfa(read_line( sys_usb_path + "/idProduct" ))
    tmp = "USB VID:PID={}:{} {}.".format(vid, pid, serial_number)
    return tmp

def get_sys_info(device_name):
    dev_name = os.path.basename(device_name)
    friendly_name = ""
    hardware_id   = ""
    sys_device_path = "/sys/class/tty/%s/device" % dev_name

    if dev_name[0:6] == "ttyUSB" :
        sys_device_path = os.path.dirname(os.path.dirname(os.path.realpath(sys_device_path)))
        if os.path.exists(sys_device_path) :
            friendly_name = usb_sysfs_friendly_name(sys_device_path)
            hardware_id   = usb_sysfs_hw_string(sys_device_path)

    return dev_name, sys_device_path, friendly_name, hardware_id

def main(argv=None):
    port_lst = port_list()
    port_lst = sorted(port_lst)
    for port in port_lst :
        dev_name, sys_device_path, friendly_name, hardware_id = get_sys_info(port)
        print("%s[%s %s]:%s" % (dev_name,friendly_name,hardware_id ,sys_device_path))

if __name__ == "__main__":
    main()